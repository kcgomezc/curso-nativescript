
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { EffectsModule } from "@ngrx/effects";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NewsService } from "./domain/news.service";
import { ReviewsService } from "./domain/reviews.service";
import { MinLenDirective } from "./minlen.validator";
import { UserComponent } from "./user/user.component";
import { initializeNoticiaState,
        NoticiasEffects,
        NoticiasState,
        reducerNoticias } from "./domain/new-state.models";

// inicia redux
export interface AppState {
    noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducerNoticias
};

const reducersInitialState = {
    noticias: initializeNoticiaState()
};

// fin redux init

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    providers: [
        NewsService,
        ReviewsService
    ],
    declarations: [
        AppComponent,
        MinLenDirective
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
