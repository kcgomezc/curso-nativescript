import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { NewsComponent } from "./news/news.component";
import { WeatherComponent } from "./weather/weather.component";
import { NewsDetComponent } from "./news-det/news-det.component";
import { ReviewComponent } from "./review/review.component";

const routes: Routes = [
    { path: "news", component: NewsComponent },
    { path: "weather", component: WeatherComponent },
    { path: "news-det", component: NewsDetComponent },
    { path: "review", component: ReviewComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NewsRoutingModule { }
