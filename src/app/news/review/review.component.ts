import { Component, OnInit } from "@angular/core";
import { ReviewsService } from "~/app/domain/reviews.service";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";
import { View } from "tns-core-modules/ui/core/view/view";
import { Color } from "tns-core-modules/color";
import { GestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout";

@Component({
  selector: "ns-review",
  templateUrl: "./review.component.html",
  styleUrls: ["./review.component.css"]
})
export class ReviewComponent implements OnInit {

  private toastOptions = Toast.makeText("Alerta: El botón fue tocado", "short");
  private toastOptions1 = Toast.makeText("Registro borrado", "short");
  private toastOptions2 = Toast.makeText("Registro archivado", "short");

  constructor(private reviews: ReviewsService) {
    // a
  }

  ngOnInit(): void {
        this.reviews.add("review 1");
        this.reviews.add("review 2");
        this.reviews.add("review 3");
  }

  onLongPress(args: GestureEventData) {
    console.log("Object that triggered the event: " + args.object);
    console.log("View that triggered the event: " + args.view);
    console.log("Event name: " + args.eventName);

    const view = <View>args.object;
    view.rotate = 0;
    view.backgroundColor = new Color("red");
    view.animate({
        rotate: 360,
        backgroundColor: new Color("green"),
        duration: 2000

    });

    this.doLater(() =>
    dialogs.action("Seleccione una opción", "Cancelar", ["Borrar", "Archivar"])
           .then((result) => {
                console.log("resultado: " + result);
                if (result === "Borrar") {
                  this.doLater(() =>
                        dialogs.alert({
                            title: "Borrar",
                            message: "Seleccionaste borrar",
                            okButtonText: "Ok"
                        }).then(() => {
                          console.log("aqui debo implementar logica de borrado");
                          this.reviews.delete("review 1");
                          this.doLater(() => this.toastOptions1.show());
                        })
                    );
                } else if (result === "Archivar") {
                    
                    this.doLater(() =>
                        dialogs.alert({
                            title: "Archivar",
                            message: "Seleccionaste archivar",
                            okButtonText: "Ok"
                        }).then(() => {
                          console.log("aqui debo implementar logica de archivar");
                          this.reviews.store("review 2");
                          this.doLater(() => this.toastOptions2.show());
                        })
                    );
                }
            })
    );
  }

  doLater(fn) {
    setTimeout(fn, 1000);
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

}
