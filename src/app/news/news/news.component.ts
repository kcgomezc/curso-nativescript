import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NewsService } from "~/app/domain/news.service";

import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "ns-news",
  templateUrl: "./news.component.html",
  styleUrls: ["./news.component.css"]
})
export class NewsComponent implements OnInit {

  constructor(private news: NewsService,
              private routerExtensions: RouterExtensions) {
    // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
    this.news.add("Noticia 1!");
    this.news.add("Noticia 2!");
    this.news.add("Noticia 3!");
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onItemTap(x): void {
  console.dir(x);
  }

  onNavItemTap(navItemRoute: string): void {
    this.routerExtensions.navigate([navItemRoute], {
        transition: {
            name: "fade"
        }
    });

    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
  }

  onPull(e) {
    const pullRefresh = e.object;
    setTimeout(() => {
      this.news.add("Nueva noticia!");
      pullRefresh.refreshing = false;
    }, 2000);
    console.log("Luego del OnPull" + this.news);
  }

}
