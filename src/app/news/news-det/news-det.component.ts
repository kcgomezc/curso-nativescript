import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import { GestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout";
import { ReviewsService } from "~/app/domain/reviews.service";

@Component({
  selector: "ns-news-det",
  moduleId: module.id,
  templateUrl: "./news-det.component.html",
  styleUrls: ["./news-det.component.css"]
})
export class NewsDetComponent implements OnInit {
  
  @ViewChild("layout", { static: false }) layout: ElementRef;
  
  private toastOptions = Toast.makeText("Alerta: El botón fue tocado", "short");
  private toastOptions1 = Toast.makeText("Action: Opción 1", "short");
  private toastOptions2 = Toast.makeText("Action: Opción 2", "short");

  constructor(private reviews: ReviewsService) {
      this.reviews.add("Review 1");
      this.reviews.add("Review 2");
      this.reviews.add("Review 3");
   }

  ngOnInit(): void {
       // Use the component constructor to inject providers.
  }

  onPull(e) {
    const pullRefresh = e.object;
    setTimeout(() => {
      this.reviews.add("New review");
      pullRefresh.refreshing = false;
    }, 2000);
    console.log("Luego del OnPull" + this.reviews);
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onTapAlert(e) {
    dialogs.alert({
      title: "Tap me!",
      message: "Mensaje 1",
      okButtonText: "Botón 1"
    }).then(() => this.doLater(() => this.toastOptions.show()));

  }
  onTapAction(e) {
      this.doLater(() =>
      dialogs.action("Mensaje", "Cancelar", ["Opción1", "Opción2"])
             .then((result) => {
                  console.log("resultado: " + result);
                  if (result === "Opción1") {
                    this.doLater(() =>
                          dialogs.alert({
                              title: "Título 1",
                              message: "Mensaje 1",
                              okButtonText: "Botón 1"
                          }).then(() => this.doLater(() => this.toastOptions1.show()))
                      );
                  } else if (result === "Opción2") {
                      
                      this.doLater(() =>
                          dialogs.alert({
                              title: "Título 2",
                              message: "Mensaje 2",
                              okButtonText: "Botón 2"
                          }).then(() => this.doLater(() => this.toastOptions2.show()))
                      );
                  }
              })
      );
  }

  doLater(fn) {
      setTimeout(fn, 1000);
  }

  // buscarAhora(s: string) {
  //   console.log("estoy buscando: " + s);
  //   console.log("array de reviews: " + this.reviews);
  //   this.resultados = this.reviews.filter((x) => x.indexOf(s) >= 0);
  //   console.log("array de resultados" + this.resultados);

  //   const layout = <View>this.layout.nativeElement;
  //   layout.animate({
  //       backgroundColor: new Color("blue"),
  //       duration: 3000,
  //       delay: 1500
  //   }).then(() => layout.animate({
  //       backgroundColor: new Color("white"),
  //       duration: 3000,
  //       delay: 1500
  //   }));
}

  // onLongPress(args: GestureEventData) {
  //   console.log("Object that triggered the event: " + args.object);
  //   console.log("View that triggered the event: " + args.view);
  //   console.log("Event name: " + args.eventName);

  //   const grid = <GridLayout>args.object;
  //   grid.rotate = 0;
  //   grid.animate({
  //       rotate: 360,
  //       duration: 2000

  //   });
  // }
