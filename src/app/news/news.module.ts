import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { NewsRoutingModule } from "./news-routing.module";
import { NewsComponent } from "./news/news.component";
import { WeatherComponent } from "./weather/weather.component";
import { NewsDetComponent } from "./news-det/news-det.component";
import { NewsDetFormComponent } from "./news-det/news-det-form.component";
import { ReviewComponent } from "./review/review.component";
import { ReviewFormComponent } from "./review/review-form.component";
// import { MinLenDirective } from "../minlen.validator";

@NgModule({
  declarations: [
    NewsComponent,
    WeatherComponent,
    NewsDetComponent,
    NewsDetFormComponent,
    ReviewComponent,
    ReviewFormComponent// ,
    // MinLenDirective
  ],
  imports: [
    NativeScriptCommonModule,
    NewsRoutingModule,
    NativeScriptFormsModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class NewsModule { }
