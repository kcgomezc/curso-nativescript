import { Component, EventEmitter, Output, Input, OnInit } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "SettingsForm",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row">
        <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..." required minlen="4"> </TextField>
        <Label *ngIf="texto.hasError('required')" text="*"></Label>
        <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"> </Label>
    </FlexboxLayout>
    <Button text="Editar nombre de usuario!" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    `
})

export class SettingsFormComponent implements OnInit {

    textFieldValue: string = "";
    @Output() changeUser: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }

    onButtonTap(): void {
        appSettings.setString("nombreUsuario", this.textFieldValue);
        console.log("nombreUsuario: " + appSettings.getString("nombreUsuario"));

        if (this.textFieldValue.length > 2) {
            this.changeUser.emit(this.textFieldValue);
        }
    }
}
