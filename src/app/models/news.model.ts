export class News {
    id: number;
    titulo: string;
    favorita: boolean;

    constructor(id: number, titulo: string) {
        this.id = id;
        this.titulo = titulo;
        this.favorita = false;
    }
}
