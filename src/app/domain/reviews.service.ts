import { Injectable } from "@angular/core";

export class ReviewsService {
    private reviews: Array<string> = [];
    private archivados: Array<string> = [];

    add(s: string) {
        this.reviews.push(s);
    }

    all() {
        return this.reviews;
    }

    delete(s: string) {
        console.log("voy a borrar a " + s);
//        this.borrados = this.reviews.splice(0, 1, s);
        this.reviews.splice(this.reviews.indexOf(s), 1);
        console.log("Luego de borrar " +  this.reviews);
    }

    store(s: string) {
        console.log("voy a archivar a " + s);
        this.archivados = this.reviews.splice(0, 1, s);
        this.reviews.splice(this.reviews.indexOf(s), 1);
        console.log("Luego de archivar " +  this.reviews);
        console.log("Los archivados " +  this.archivados);

    }
}
