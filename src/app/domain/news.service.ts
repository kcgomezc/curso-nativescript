import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NewsService {

    api: string  = "https://b4fc6d83bc5b.ngrok.io";
    // database: couchbaseModule.Couchbase;

    constructor() {

        // this.database = new couchbaseModule.Couchbase("test-database");

        this.getDB((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("Fila: ", fila),
                (err, totales) => console.log("Filas totales", totales)
            );
        }, () => console.log("Error on getDB"));

        // this.database.createView("logs", "1", (document, emitter) =>
        // emitter.emit(document._id, document));
        // const rows = this.database.executeQuery("logs", {limit : 200});
        // console.log("documentos: " + JSON.stringify(rows));
    }

    getDB(fnOk, fnError) {
        return new sqlite("my_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir DB!", err);
            } else {
                console.log("BD abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    searchDB(s: string) {
        this.getDB((db) => {
            db.execSQL("INSERT INTO logs (texto) VALUES (?)", [s],
                (err, id) => console.log("Nuevo id: ", id)
            );
        }, () => console.log("Error on buscarDB"));

        // const documentId = this.database.createDocument({ texto: s });
        // console.log("nuevo id couchbase: ", documentId);

        return getJSON(this.api + "/get?q=" + s);
    }

    add(s: string) {
        
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: {"Content-Type": "application/json"},
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    search(s: string) {
        return getJSON(this.api + "/get?q=" + s);
    }

    postFavorita(n: string) {
        console.log("dentro del post" + n);

        this.getDB((db) => {
            db.execSQL("INSERT INTO logs (texto) VALUES (?)", [n],
                (err, id) => console.log("Nuevo id: ", id)
            );
        }, () => console.log("Error on buscarDB"));

        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: n
            })
        });
    }

    deleteFavorita(n: string) {
        console.log("dentro del delete" + n);

        this.getDB((db) => {
            db.execSQL("INSERT INTO logs (texto) VALUES (?)", [n],
                (err, id) => console.log("Nuevo id: ", id)
            );
        }, () => console.log("Error on buscarDB"));

        return request({
            url: this.api + "/favs",
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                favorita: n
            })
        });
    }

}
