import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

// ESTADO
export class Noticia {
   constructor(public titulo: string) {}
}

// tslint:disable-next-line: max-classes-per-file
export interface NoticiasState {
   items: Array<Noticia>;
   sugerida: Noticia;
   leyendo: Array<Noticia>;
}

export function initializeNoticiaState() {
    return {
        items: [],
        sugerida: null,
        leyendo: null
    };
}

// ACCIONES
export enum NoticiasActionTypes {
    INIT_MY_DATA = "[Noticias] Init My Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] Sugerir",
    LEER_NOTICIA = "[Noticias] Leer"
}

// tslint:disable-next-line:max-classes-per-file
export class InitMyDataAction implements Action {
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: Array<string>) { }
}

// tslint:disable-next-line: max-classes-per-file
export class NuevaNoticiaAction implements Action {
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia: Noticia) { }
}

// tslint:disable-next-line: max-classes-per-file
export class SugerirNoticiaAction implements Action {
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia: Noticia) { }
}

// tslint:disable-next-line: max-classes-per-file
export class LeerNoticiaAction implements Action {
    type = NoticiasActionTypes.LEER_NOTICIA;
    constructor(public noticia: Noticia) { }
}

export type NoticiasActions = InitMyDataAction | NuevaNoticiaAction | SugerirNoticiaAction | LeerNoticiaAction;

// REDUCERS
export function reducerNoticias(
    state: NoticiasState,
    action: NoticiasActions
): NoticiasState {
    switch (action.type) {
        case NoticiasActionTypes.INIT_MY_DATA: {
            console.log("reducerNoticias - INIT_MY_DATA");
            const titulares: Array<string> = (action as InitMyDataAction).titulares;
            console.log(titulares);
            
            return {
                ...state,
                items: titulares.map((t) => new Noticia(t))
            };
        }
        case NoticiasActionTypes.NUEVA_NOTICIA: {
            console.log("reducerNoticias - NUEVA_NOTICIA");
            
            return {
                ...state,
                items: [...state.items, (action as NuevaNoticiaAction).noticia]
            };
        }
        case NoticiasActionTypes.SUGERIR_NOTICIA: {
            console.log("reducerNoticias - SUGERIR_NOTICIA");

            return {
                ...state,
                sugerida: (action as SugerirNoticiaAction).noticia
            };
        }
        case NoticiasActionTypes.LEER_NOTICIA: {
            console.log("reducerNoticias - LEER_NOTICIA");

            return {
                ...state // ,
                // leyendo: (action as LeerNoticiaAction).noticia
            };
        }
    }

    return state;
}

// EFFECTS
// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class NoticiasEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe (
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        map((action: NuevaNoticiaAction) => new LeerNoticiaAction(action.noticia))
    );

    constructor(private actions$: Actions) { }
}
