import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import * as Toast from "nativescript-toast";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/new-state.models";
import { NewsService } from "../domain/news.service";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import { GestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { ImageSource } from "tns-core-modules/image-source";
import { compose } from "nativescript-email";


@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
    // ,
    // providers: [NewsService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    favoritos: Array<string>;
    isAndroid: boolean = false;
    @ViewChild("layout", { static: false }) layout: ElementRef;

    constructor(private news: NewsService,
                private store: Store<AppState>) {
        
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if (f != null) {
                const toastOptions = Toast.makeText("Sugerimos leer" + f.titulo);
                this.doLater(() =>
                    toastOptions.show()
                );
            }
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    buscarAhora(s: string) {
        console.log("buscarAhora " +  s);
        // console.log(this.news);
        // this.resultados = this.news.search().filter((x) => x.indexOf(s) >= 0);

        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 3000,
        //     delay: 1500
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 3000,
        //     delay: 1500
        // }));

        this.news.searchDB(s).then((r: any) => {
            console.log("resultados buscarAhora: " +  JSON.stringify(r));
            this.resultados = r;
        }, (e)  => {
            console.log("error buscarAhora " + e);
            const toastOptions = Toast.makeText("Eror en la búsqueda");
            this.doLater(() =>
                toastOptions.show()
            );
        });
    }

    doLater(fn) {
        setTimeout(fn, 1000);
    }

    onTapAddFav(args: GestureEventData, x: string) {
        console.log("Tap");
        console.log("Object that triggered the event: " + args.object);
        console.log("View that triggered the event: " + args.view);
        console.log("Event name: " + args.eventName);
        this.news.postFavorita(x);
        Toast.makeText("'" + x + "' se agregó a Favoritos", "short").show();
    }

    onLongPress(s): void {
        console.log(s);
        // SocialShare.shareText(s, "Asunto: I love NativeScript!");
        // let image = ImageSource.fromFileSync("~/images/android_logo.jpg");
        // SocialShare.shareImage(image);

        const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
        const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
        const appPath = appFolder.path; // esto te da el path a la carpeta src
        const logoPath = appPath + "/app/res/android_logo.jpg"; // aquí armas el path del archivo copiado
        compose({
            subject: "Mail de Prueba", // asunto del mail
            body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
            to: ["katerinegomez@gmail.com"], // lista de destinatarios principales
            cc: ["julianamunera@gmail.com"], // lista de destinatarios en copia
            bcc: [], // lista de destinatarios en copia oculta
            attachments: [ // listado de archivos adjuntos
            {
                fileName: "arrow1.png", // este archivo adjunto está en formato base 64 representado por un string
                path: "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC",
                mimeType: "image/png"
            },
            {
                fileName: "icon.png", // este archivo es el que lees directo del filesystem del mobile
                path: logoPath,
                mimeType: "image/png"
            }]
        }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err));

    }
}
